#!/bin/sh

NODE_VERSION=${1:-"6.9.4"}

if [ ! -d venv ]; then
    for VIRTUALENV in virtualenv2 virtualenv; do
	>/dev/null which $VIRTUALENV 2>&1
	if [ $? -eq 0 ]; then
	    # XXX find a portable way setup a virtual env
	    $VIRTUALENV venv
	    break
	fi
    done
    if [ ! -d venv ]; then
	echo "Error: failed to create virtual env"
	exit 2
    fi
fi
source venv/bin/activate

cd venv/
export PREFIX=`pwd`
cd ..

if [ ! -f node-v"$NODE_VERSION".tar.gz ]; then
    >/dev/null which wget 2>&1
    if [ $? -ne 0 ]; then
	echo "Error: wget not available"
	exit 1
    fi
    wget http://nodejs.org/dist/v"$NODE_VERSION"/node-v"$NODE_VERSION".tar.gz
    tar xvf node-v"$NODE_VERSION".tar.gz
fi

if [ "X$(which node)" != "X$(pwd)/venv/bin/node" ]; then
    cd node-v"$NODE_VERSION"
    ./configure --prefix=$PREFIX
    if [ $? -ne 0 ]; then
	exit -1
    fi
    make -j4
    make install
fi

echo "Done!"
