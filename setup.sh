#!/bin/sh

APP=${1:-server}
HISTORY=".history"

if [ -d venv ]; then
    source venv/bin/activate
else
    echo "Please run node_install"
    return 1
fi

CMD=""
if [ -f  $HISTORY ]; then
    CMD=$(cat $HISTORY)
fi

PROMPT="Command to run inside $APP"
if [ ! -z "$CMD" ]; then
    PROMPT="$PROMPT ($CMD)"
fi

read -p "$PROMPT: " cmd

if [ -z "$cmd" ]; then
    if [ ! -z "$CMD" ]; then
	cmd="$CMD"
    else
	exit 1
    fi
else
    echo "$cmd" > $HISTORY
fi

if [ -d "$APP" ]; then
    cd $APP
    $cmd
fi

echo "Done!"
